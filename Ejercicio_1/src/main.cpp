#include <Arduino.h>

//Numero de los Pines
#define PIN_ROJO      5
#define PIN_AMARILLO  6
#define PIN_VERDE     7

//Logica a usar
#define ACTIVADO      HIGH
#define DESACTIVADO   LOW

//Milisegundos que cada luz estara encendida
#define TIEMPO_ROJO_MS          31000
#define TIEMPO_AMARILLO_MS      3000
#define TIEMPO_ROJOAMARILLO_MS  3000
#define TIEMPO_VERDE_MS         25000


typedef enum 
{
  ESTADO_ROJO = 0,
  ESTADO_ROJOAMARILLO,
  ESTADO_AMARILLO,
  ESTADO_VERDE,
  PULSADOR
} estado_semaforo;

  estado_semaforo estado = ESTADO_AMARILLO;


void setup() {
  
  //Declaracion de los pines
  pinMode (PIN_ROJO,      OUTPUT);
  pinMode (PIN_AMARILLO,  OUTPUT);
  pinMode (PIN_VERDE,     OUTPUT);

  //Inicializo los pines en bajo
  digitalWrite (PIN_ROJO,       DESACTIVADO);
  digitalWrite (PIN_AMARILLO,   DESACTIVADO);
  digitalWrite (PIN_VERDE,      DESACTIVADO);

}

void loop() {


  switch (estado)
  {
  case ESTADO_AMARILLO:
    digitalWrite (PIN_ROJO    , DESACTIVADO);
    digitalWrite (PIN_AMARILLO, ACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
    delay (TIEMPO_AMARILLO_MS);
    estado = ESTADO_ROJO;
  break;

  case ESTADO_ROJO:
    digitalWrite (PIN_ROJO    , ACTIVADO);
    digitalWrite (PIN_AMARILLO, DESACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
    delay (TIEMPO_ROJO_MS);
    estado = ESTADO_ROJOAMARILLO;
  break;

  case ESTADO_ROJOAMARILLO:
    digitalWrite (PIN_ROJO    , ACTIVADO);
    digitalWrite (PIN_AMARILLO, ACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
    delay (TIEMPO_ROJOAMARILLO_MS);
    estado = ESTADO_VERDE;
  break;

  case ESTADO_VERDE:
    digitalWrite (PIN_ROJO    , DESACTIVADO);
    digitalWrite (PIN_AMARILLO, DESACTIVADO);
    digitalWrite (PIN_VERDE   , ACTIVADO);
    delay (TIEMPO_VERDE_MS);
    estado = ESTADO_AMARILLO;
  break;
  
  default:
    estado = ESTADO_AMARILLO;
  break;
  }

  

}