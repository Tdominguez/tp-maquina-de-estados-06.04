#include <Arduino.h>

//Numero de los Pines
#define PIN_ROJO      5
#define PIN_AMARILLO  6
#define PIN_VERDE     7

//Logica a usar
#define ACTIVADO      HIGH
#define DESACTIVADO   LOW

//Base de tiempo de la ISR
#define CONTADOR_ISR  30 //0.9792 segundos (32.64ms * 30)

//Segundos que cada luz estara encendida
#define ISR_TIEMPO_ROJO          31
#define ISR_TIEMPO_AMARILLO      3
#define ISR_TIEMPO_ROJOAMARILLO  3
#define ISR_TIEMPO_VERDE         25


//ISR
volatile unsigned int cuenta = 0;
int contador_segundos        = 0;


typedef enum 
{
  ESTADO_ROJO = 0,
  ESTADO_ROJOAMARILLO,
  ESTADO_AMARILLO,
  ESTADO_VERDE
} estado_semaforo;

  estado_semaforo estado = ESTADO_AMARILLO;


void setup() {
  
  //ISR
  SREG = (SREG & 0b01111111);               //Desabilitar interrupciones
  TIMSK2 = TIMSK2|0b00000001;               //Habilita la interrupcion por desbordamiento
  TCCR2B = 0b00000111;                      //Configura preescala para que FT2 sea de 7812.5Hz
  SREG = (SREG & 0b01111111) | 0b10000000;  //Habilitar interrupciones
  
  //Declaracion de los pines
  pinMode (PIN_ROJO,      OUTPUT);
  pinMode (PIN_AMARILLO,  OUTPUT);
  pinMode (PIN_VERDE,     OUTPUT);

  //Inicializo los pines en bajo
  digitalWrite (PIN_ROJO,       DESACTIVADO);
  digitalWrite (PIN_AMARILLO,   DESACTIVADO);
  digitalWrite (PIN_VERDE,      DESACTIVADO);

}

void loop() {


  switch (estado)
  {
  case ESTADO_AMARILLO:
    if (contador_segundos == ISR_TIEMPO_AMARILLO)
    {
      estado = ESTADO_ROJO;
      contador_segundos = 0;
    }
  break;

  case ESTADO_ROJO:
    if (contador_segundos == ISR_TIEMPO_ROJO)
    {
      estado = ESTADO_ROJOAMARILLO;
      contador_segundos = 0;
    }
  break;

  case ESTADO_ROJOAMARILLO:
    if (contador_segundos == ISR_TIEMPO_ROJOAMARILLO)
    {
      estado = ESTADO_VERDE;
      contador_segundos = 0;
    }
  break;

  case ESTADO_VERDE:
    if (contador_segundos == ISR_TIEMPO_VERDE)
    {
      estado = ESTADO_AMARILLO;
      contador_segundos = 0;
    }
  }

  switch (estado)
  {
  case ESTADO_AMARILLO:
    digitalWrite (PIN_ROJO    , DESACTIVADO);
    digitalWrite (PIN_AMARILLO, ACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
  break;

  case ESTADO_ROJO:
    digitalWrite (PIN_ROJO    , ACTIVADO);
    digitalWrite (PIN_AMARILLO, DESACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
  break;

  case ESTADO_ROJOAMARILLO:
    digitalWrite (PIN_ROJO    , ACTIVADO);
    digitalWrite (PIN_AMARILLO, ACTIVADO);
    digitalWrite (PIN_VERDE   , DESACTIVADO);
  break;

  case ESTADO_VERDE:
    digitalWrite (PIN_ROJO    , DESACTIVADO);
    digitalWrite (PIN_AMARILLO, DESACTIVADO);
    digitalWrite (PIN_VERDE   , ACTIVADO);
  break;
    
    contador_segundos = 0;

  break;
  
  default:
    estado = ESTADO_AMARILLO;
  break;
  }

}


ISR(TIMER2_OVF_vect){
    
    cuenta++;
    
    if(cuenta > CONTADOR_ISR) 
    {
      contador_segundos ++;
      cuenta=0;
    }

    }